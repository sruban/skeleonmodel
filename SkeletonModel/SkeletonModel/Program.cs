
using System;

[assembly: CLSCompliant(true)]

namespace Model
{
    /// <summary>
    /// The base Xna program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// This method starts the game cycle.
        /// </summary>
        public static void Main()
        {
            using (Game1 game = new Game1())
            {
                game.Run();
            }
        }
    }
}
﻿
namespace Model
{
    using System;
    using Microsoft.Kinect;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;

    /// <summary>
    /// A delegate method explaining how to map a SkeletonPoint from one space to another.
    /// </summary>
    /// <param name="point">The SkeletonPoint to map.</param>
    /// <returns>The Vector2 representing the target location.</returns>
    public delegate Vector2 SkeletonPointMap(SkeletonPoint point);

    /// <summary>
    /// This class is responsible for rendering a skeleton stream.
    /// </summary>
    public class SkeletonStreamRenderer : Object2D
    {
        /// <summary>
        /// This is the map method called when mapping from
        /// skeleton space to the target space.
        /// </summary>
        private readonly SkeletonPointMap mapMethod;

        /// <summary>
        /// The last frames skeleton data.
        /// </summary>
        private static Skeleton[] skeletonData;

        /// <summary>
        /// This flag ensures only request a frame once per update call
        /// across the entire application.
        /// </summary>
        private static bool skeletonDrawn = true;

        /// <summary>
        /// The origin (center) location of the joint texture.
        /// </summary>
        private Vector2 jointOrigin;

        /// <summary>
        /// The joint texture.
        /// </summary>
        private Texture2D jointTexture;

        /// <summary>
        /// The origin (center) location of the bone texture.
        /// </summary>
        private Vector2 boneOrigin;

        /// <summary>
        /// The bone texture.
        /// </summary>
        private Texture2D boneTexture;


        private Texture2D bodyTexture;

        private Texture2D head;
        private Texture2D ShoulderLeft;
        private Texture2D ShoulderRight;
        private Texture2D Spine;
        private Texture2D HipCenter;
        private Texture2D HipLeft;
        private Texture2D HipRight;

        private Texture2D ElbowLeft;
        private Texture2D WristLeft;
        private Texture2D HandLeft;


        private Texture2D ElbowRight;
        private Texture2D WristRight;
        private Texture2D HandRight;

        private Texture2D KneeLeft;
        private Texture2D AnkleLeft;
        private Texture2D FootLeft;

        private Texture2D KneeRight;
        private Texture2D AnkleRight;
        private Texture2D FootRight;


        /// <summary>
        /// Whether the rendering has been initialized.
        /// </summary>
        private bool initialized;

        /// <summary>
        /// Initializes a new instance of the SkeletonStreamRenderer class.
        /// </summary>
        /// <param name="game">The related game object.</param>
        /// <param name="map">The method used to map the SkeletonPoint to the target space.</param>
        public SkeletonStreamRenderer(Game game, SkeletonPointMap map)
            : base(game)
        {
            this.mapMethod = map;
        }

        /// <summary>
        /// This method initializes necessary values.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
            this.Size = new Vector2(Game.GraphicsDevice.Viewport.Width, Game.GraphicsDevice.Viewport.Height);
            this.initialized = true;
        }

        /// <summary>
        /// This method retrieves a new skeleton frame if necessary.
        /// </summary>
        /// <param name="gameTime">The elapsed game time.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // If the sensor is not found, not running, or not connected, stop now
            if (null == this.Chooser.Sensor ||
                false == this.Chooser.Sensor.IsRunning ||
                KinectStatus.Connected != this.Chooser.Sensor.Status)
            {
                return;
            }

            // If we have already drawn this skeleton, then we should retrieve a new frame
            // This prevents us from calling the next frame more than once per update
            if (skeletonDrawn)
            {
                using (var skeletonFrame = this.Chooser.Sensor.SkeletonStream.OpenNextFrame(0))
                {
                    // Sometimes we get a null frame back if no data is ready
                    if (null == skeletonFrame)
                    {
                        return;
                    }

                    // Reallocate if necessary
                    if (null == skeletonData || skeletonData.Length != skeletonFrame.SkeletonArrayLength)
                    {
                        skeletonData = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    }

                    skeletonFrame.CopySkeletonDataTo(skeletonData);
                    skeletonDrawn = false;
                }
            }
        }

        /// <summary>
        /// This method draws the skeleton frame data.
        /// </summary>
        /// <param name="gameTime">The elapsed game time.</param>
        public override void Draw(GameTime gameTime)
        {
            // If the joint texture isn't loaded, load it now
            if (null == this.jointTexture)
            {
                this.LoadContent();
            }

            // If we don't have data, lets leave
            if (null == skeletonData || null == this.mapMethod)
            {
                return;
            }

            if (false == this.initialized)
            {
                this.Initialize();
            }

            this.SharedSpriteBatch.Begin();

            foreach (var skeleton in skeletonData)
            {
                switch (skeleton.TrackingState)
                {
                    case SkeletonTrackingState.Tracked:
                        // Draw Bones
                        this.DrawBone(skeleton.Joints, JointType.Head, JointType.ShoulderCenter, head);
                        //this.DrawBone(skeleton.Joints, JointType.ShoulderCenter, JointType.ShoulderLeft,ShoulderLeft);
                        //this.DrawBone(skeleton.Joints, JointType.ShoulderCenter, JointType.ShoulderRight,ShoulderRight);
                        this.DrawBone(skeleton.Joints, JointType.ShoulderCenter, JointType.Spine, Spine);
                        this.DrawBone(skeleton.Joints, JointType.Spine, JointType.HipCenter, HipCenter);
                        //this.DrawBone(skeleton.Joints, JointType.HipCenter, JointType.HipLeft,HipLeft);
                        //this.DrawBone(skeleton.Joints, JointType.HipCenter, JointType.HipRight,HipRight);

                        this.DrawBone(skeleton.Joints, JointType.ShoulderLeft, JointType.ElbowLeft, ElbowLeft);
                        this.DrawBone(skeleton.Joints, JointType.ElbowLeft, JointType.WristLeft, WristLeft);
                        this.DrawBone(skeleton.Joints, JointType.WristLeft, JointType.HandLeft, HandLeft);

                        this.DrawBone(skeleton.Joints, JointType.ShoulderRight, JointType.ElbowRight, ElbowRight);
                        this.DrawBone(skeleton.Joints, JointType.ElbowRight, JointType.WristRight, WristRight);
                        this.DrawBone(skeleton.Joints, JointType.WristRight, JointType.HandRight, HandRight);

                        this.DrawBone(skeleton.Joints, JointType.HipLeft, JointType.KneeLeft, KneeLeft);
                        this.DrawBone(skeleton.Joints, JointType.KneeLeft, JointType.AnkleLeft, AnkleLeft);
                        this.DrawBone(skeleton.Joints, JointType.AnkleLeft, JointType.FootLeft, FootLeft);

                        this.DrawBone(skeleton.Joints, JointType.HipRight, JointType.KneeRight, KneeRight);
                        this.DrawBone(skeleton.Joints, JointType.KneeRight, JointType.AnkleRight, AnkleRight);
                        this.DrawBone(skeleton.Joints, JointType.AnkleRight, JointType.FootRight, FootRight);

                        // Now draw the joints
                        foreach (Joint j in skeleton.Joints)
                        {
                            Color jointColor = Color.Green;
                            if (j.TrackingState != JointTrackingState.Tracked)
                            {
                                jointColor = Color.Yellow;
                            }

                            this.SharedSpriteBatch.Draw(
                                this.jointTexture,
                                this.mapMethod(j.Position),
                                null,
                                jointColor,
                                0.0f,
                                this.jointOrigin,
                                1.0f,
                                SpriteEffects.None,
                                0.0f);
                        }

                        break;
                    case SkeletonTrackingState.PositionOnly:
                        // If we are only tracking position, draw a blue dot
                        this.SharedSpriteBatch.Draw(
                                this.jointTexture,
                                this.mapMethod(skeleton.Position),
                                null,
                                Color.Blue,
                                0.0f,
                                this.jointOrigin,
                                1.0f,
                                SpriteEffects.None,
                                0.0f);
                        break;
                }
            }

            this.SharedSpriteBatch.End();
            skeletonDrawn = true;

            base.Draw(gameTime);
        }

        /// <summary>
        /// This method loads the textures and sets the origin values.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();

            this.jointTexture = Game.Content.Load<Texture2D>("Joint");
            this.jointOrigin = new Vector2(this.jointTexture.Width / 2, this.jointTexture.Height / 2);

            this.head = Game.Content.Load<Texture2D>("fish");
            //this.ShoulderLeft = Game.Content.Load<Texture2D>("fish");
            //this.ShoulderRight = Game.Content.Load<Texture2D>("fish");
            this.Spine = Game.Content.Load<Texture2D>("fish");
            this.HipCenter = Game.Content.Load<Texture2D>("fish");
            //this.HipLeft = Game.Content.Load<Texture2D>("fish");
            //this.HipRight = Game.Content.Load<Texture2D>("fish");

            this.ElbowLeft = Game.Content.Load<Texture2D>("fish");
            this.WristLeft = Game.Content.Load<Texture2D>("fish");
            this.HandLeft = Game.Content.Load<Texture2D>("fish");

            this.ElbowRight = Game.Content.Load<Texture2D>("fish");
            this.WristRight = Game.Content.Load<Texture2D>("fish");
            this.HandRight = Game.Content.Load<Texture2D>("fish");


            this.KneeLeft = Game.Content.Load<Texture2D>("fish");
            this.AnkleLeft = Game.Content.Load<Texture2D>("fish");
            this.FootLeft = Game.Content.Load<Texture2D>("fish");

            this.KneeRight = Game.Content.Load<Texture2D>("fish");
            this.AnkleRight = Game.Content.Load<Texture2D>("fish");
            this.FootRight = Game.Content.Load<Texture2D>("fish");



            this.boneOrigin = new Vector2(0.5f, 0.0f);
        }

        /// <summary>
        /// This method draws a bone.
        /// </summary>
        /// <param name="joints">The joint data.</param>
        /// <param name="startJoint">The starting joint.</param>
        /// <param name="endJoint">The ending joint.</param>
        private void DrawBone(JointCollection joints, JointType startJoint, JointType endJoint, Texture2D bodypart)
        {
            Vector2 start = this.mapMethod(joints[startJoint].Position);
            Vector2 end = this.mapMethod(joints[endJoint].Position);
            Vector2 diff = end - start;
            boneTexture = bodypart;
            Vector2 scale = new Vector2(diff.Length() / boneTexture.Height, diff.Length() / boneTexture.Height);
            //Vector2 scale = new Vector2(boneTexture.Width/4, boneTexture.Height / 2);

            float angle = (float)Math.Atan2(diff.Y, diff.X) - MathHelper.PiOver2 + (float)(Math.PI / 2);

            Color color = Color.White;
            if (joints[startJoint].TrackingState != JointTrackingState.Tracked ||
                joints[endJoint].TrackingState != JointTrackingState.Tracked)
            {
                color = Color.Gray;
            }

            // this.SharedSpriteBatch.Draw(this.boneTexture, start, null, color, angle, this.boneOrigin, scale, SpriteEffects.None, 1.0f);
            this.SharedSpriteBatch.Draw(boneTexture, start, null, color, angle, this.boneOrigin, scale, SpriteEffects.FlipHorizontally, 1.0f);
        }
    }
}
